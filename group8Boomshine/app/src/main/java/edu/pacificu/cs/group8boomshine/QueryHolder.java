package edu.pacificu.cs.group8boomshine;

/**
 * Functionally a struct to hold query results
 *
 * @author David Wright
 * @version 1.0
 */

import java.util.ArrayList;

public class QueryHolder {
    public ArrayList<Integer> scores;
    public ArrayList<String> players;

    /**
     * Initializes ArrayLists
     */
    QueryHolder() {
        scores = new ArrayList<>();
        players = new ArrayList<>();
    }
}
