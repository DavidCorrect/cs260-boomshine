package edu.pacificu.cs.group8boomshine;

/**
 * Helper class that sets up database creation, insert, and delete statements along with
 * onCreate handling
 *
 * @author David Wright
 * @version 1.0
 */

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class HSDbHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "HighScores.db";

    private static final String SQL_CREATE_ENTRIES = "CREATE TABLE " +
            HSContract.HighScores.TABLE_NAME + " (" + HSContract.HighScores._ID +
            " INTEGER PRIMARY KEY, " + HSContract.HighScores.COLUMN_NAME_SCORE +
            " INTEGR, " + HSContract.HighScores.COLUMN_NAME_PLAYER + " TEXT)";

    private final String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " +
            HSContract.HighScores.TABLE_NAME;

    /**
     * Calls super constructor to initialize database
     *
     * @param context view context
     */
    public HSDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * Inilization on creation of database
     *
     * @param db current database
     */
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_ENTRIES);
    }

    /**
     * Handles if you upgrade the database version
     *
     * @param db         current database
     * @param oldVersion old db version
     * @param newVersion new db version
     */
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }
}
