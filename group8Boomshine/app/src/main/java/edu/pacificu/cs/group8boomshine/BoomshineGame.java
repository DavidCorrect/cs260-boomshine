package edu.pacificu.cs.group8boomshine;

/**
 * Defines the BoomshineGame class that runs different boomshine levels while checking for win
 * or loss conditions.
 *
 * @author Logan Jepson, David Wright
 * @version 1.0
 */

import android.content.Context;
import android.graphics.Canvas;
import android.view.Display;

public class BoomshineGame {
    static public int ONGOING = 0;
    static public int WON = 1;
    static public int LOST = 2;
    static private int MAX_NUM_TURNS = 2;
    Level cmCurrentLevel;
    int turnCounter;
    int gameScore;
    Context mContextRef;
    Display mDisplayRef;
    private int levelCounter = 0;

    /**
     * Constructor that initializes the values associated with the the Boomshine Game
     *
     * @param context reference to application-specific resources
     * @param display the display
     * @since 1.0
     */

    public BoomshineGame(Context context, Display display) {
        cmCurrentLevel = new Level(context, display, levelCounter);
        turnCounter = 0;
        mContextRef = context;
        mDisplayRef = display;

    }

    /**
     * runLevel updates the game level, checks the level status, and paints sprites to the
     * screen.
     *
     * @since 1.0
     */

    public int runLevel() {
        int returnState = ONGOING;

        if (cmCurrentLevel.checkLevelStatus()) {
            if (cmCurrentLevel.checkWIn()) {
                returnState = WON;
                gameScore += cmCurrentLevel.getScore();
                turnCounter = 0;
            } else if (turnCounter < MAX_NUM_TURNS) {
                cmCurrentLevel.restartLevel();
                turnCounter++;
            } else {
                returnState = LOST;
            }
        } else {
            cmCurrentLevel.updateSprites();
        }
        return returnState;
    }

    /**
     * goToNextLevel ups the level difficulty, and creates a new level object.
     *
     * @since 1.0
     */

    public void goToNextLevel() {
        levelCounter++;
        turnCounter = 0;
        cmCurrentLevel = new Level(mContextRef, mDisplayRef, levelCounter);
        cmCurrentLevel.upExpandSpeed();
    }

    /**
     * userPlacedCircle call the placeUserCirc in the level class
     *
     * @param xCord the x coordinate where the users circle will be placed.
     * @param yCord the y coordinate where the users circle will be placed.
     * @since 1.0
     */

    public void userPlacedCircle(int xCord, int yCord) {
        cmCurrentLevel.placeUserCirc(xCord, yCord);
    }

    /**
     * gemGameScore returns the current score for the boomshine game
     *
     * @since 1.0
     */

    public int getGameScore() {
        return gameScore;
    }

    /**
     * getLevel returns the current level that the game is at.
     *
     * @since 1.0
     */

    public int getLevel() {
        return levelCounter;
    }

    /**
     * getAttempt returns the attempt on the level that the user is currently at
     *
     * @since 1.0
     */

    public int getAttempt() {
        return turnCounter;
    }

    /**
     * getNumBallsToWIn returns the number of balls left required to win the game.
     *
     * @since 1.0
     */


    public int getNumBallsToWin() {
        return cmCurrentLevel.getmNumBallsToWin();
    }

    /**
     * printSprites prints the current ball sprites in the level
     *
     * @since 1.0
     */

    public void printSprites(Canvas canvas) {
        cmCurrentLevel.printSpirtes(canvas);
    }
}
