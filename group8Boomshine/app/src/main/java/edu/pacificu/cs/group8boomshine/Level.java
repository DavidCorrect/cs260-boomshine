package edu.pacificu.cs.group8boomshine;

/**
 * Defines the Level class with all data and methods associated with it/
 *
 * @author Logan Jepson, David Wright
 * @version 1.0
 */

import android.content.Context;
import android.graphics.Canvas;
import android.view.Display;

import java.util.ArrayList;
import java.util.Random;

public class Level {
    static private int BALL_DIFF_MULTIPLIER = 5;
    static private int BASE_NUM_BALLS_TO_WIN = 1;
    static private int LEVEL_BEFORE_SLOWDOWN = 3;
    static private int LEVELS_PER_BALL_CHANGE = 10;
    static private double BALL_STARTING_SCALE = 20;
    static private double BALL_OLD_STARTING_SCALE = 20;
    static private int STANDARD_SCALE;
    static private int MAX_NUM_RANDOM_COLORS = 6;
    static private int SCORE_INCRAMENTS = 10;
    static private int MAX_RANDOM = 1000;
    static private int DIV_NUM = 9;
    static private double JUST_ONE = 1.0;
    private int mCurrentDifficulty;
    private int mNumBallsToWin;
    private int mNumMovingBalls;
    private int mNumExpandingBalls;
    private int mLevelScore;
    private boolean mbLevelOver;
    private boolean mbBallPlaced;

    ArrayList<BoundedMovingSprite> maMovingSprites = new ArrayList();
    ArrayList<ExpandingSprite> maExplodingSprites = new ArrayList();
    //change later
    private Random mRand = new Random(Double.doubleToLongBits(Math.random()));
    //display stuff
    private Context mContextRef;
    private Display mDisplayRef;

    /**
     * Constructor that initializes the values and ball objects
     *
     * @param context reference to application-specific resources
     * @param display the display
     * @since 1.0
     */

    public Level(Context context, Display display) {
        mLevelScore = 0;
        mbLevelOver = false;
        mContextRef = context;
        mDisplayRef = display;
        mbBallPlaced = false;
        mNumMovingBalls = BALL_DIFF_MULTIPLIER;
        mCurrentDifficulty = 0;
        mNumExpandingBalls = -1;
        STANDARD_SCALE = 6;
        mNumBallsToWin = mNumMovingBalls - 1;
        // just for level 1
        if (BALL_STARTING_SCALE == BALL_OLD_STARTING_SCALE) {
            setBallStartingScale();
        }
        for (int i = 0; i < mNumMovingBalls; i++) {
            maMovingSprites.add(new BoundedMovingSprite(context,
                    display, getRandomDrawable(), mRand.nextInt
                    (context.getResources().getDisplayMetrics().heightPixels -
                            (int) BALL_STARTING_SCALE -
                            context.getResources().getDisplayMetrics().heightPixels / DIV_NUM),
                    mRand.nextInt(context.getResources().getDisplayMetrics().widthPixels -
                            (int) BALL_STARTING_SCALE), (int) BALL_STARTING_SCALE));
        }
    }

    /**
     * Constructor that initializes the values and game
     * object of the Expanding Sprite class.
     *
     * @param context    reference to application-specific resources
     * @param display    the display
     * @param difficulty
     * @since 1.0
     */

    public Level(Context context, Display display, int difficulty) {
        mLevelScore = 0;
        mbLevelOver = false;
        mContextRef = context;
        mDisplayRef = display;
        mNumBallsToWin = difficulty + BASE_NUM_BALLS_TO_WIN;
        mCurrentDifficulty = difficulty;
        mNumExpandingBalls = -1;
        STANDARD_SCALE = 6;
        if (BALL_STARTING_SCALE == BALL_OLD_STARTING_SCALE) {
            setBallStartingScale();
        }
        if (difficulty <= LEVEL_BEFORE_SLOWDOWN) {
            mNumMovingBalls = (difficulty + 1) * BALL_DIFF_MULTIPLIER;
        } else {
            mNumMovingBalls = 0;
            mNumMovingBalls += LEVEL_BEFORE_SLOWDOWN * BALL_DIFF_MULTIPLIER;
            mNumMovingBalls += (BALL_DIFF_MULTIPLIER * (difficulty / LEVELS_PER_BALL_CHANGE));
        }
        mNumBallsToWin = mNumMovingBalls - mNumBallsToWin;
        for (int i = 0; i < mNumMovingBalls; i++) {
            maMovingSprites.add(new BoundedMovingSprite(context,
                    display, getRandomDrawable(),
                    mRand.nextInt(context.getResources().getDisplayMetrics().heightPixels -
                            (int) BALL_STARTING_SCALE -
                            context.getResources().getDisplayMetrics().heightPixels / DIV_NUM),
                    mRand.nextInt(context.getResources().getDisplayMetrics().widthPixels - (
                            int) BALL_STARTING_SCALE), (int) BALL_STARTING_SCALE));
        }
    }

    /**
     * Return level's score
     *
     * @return Level score
     */

    public int getScore() {
        return mLevelScore;
    }

    /**
     * place user circ places an expandning circle at the coordinates provided by the user
     *
     * @param xPos x position of the expanding circle
     * @param yPos y position of the expanding circle
     */

    public void placeUserCirc(int xPos, int yPos) {
        if (!mbBallPlaced) {
            maExplodingSprites.add(new ExpandingSprite(mContextRef, mDisplayRef,
                    getRandomDrawable(), xPos, yPos, (int) BALL_STARTING_SCALE));
            mNumExpandingBalls = 1;
            mbBallPlaced = true;
        }
    }

    /**
     * update sprites updates the locations, states, and direction of all the moving and expanding
     * sprites
     */

    public void updateSprites() {
        int holderX1, holderX2, holderY1, holderY2;
        if (mNumExpandingBalls <= 0) {
            if (mbBallPlaced) {
                mbLevelOver = true;
            } else {
                for (int i = 0; i < mNumMovingBalls; i++) {
                    maMovingSprites.get(i).move();
                }
            }
        } else {
            int i = 0;
            int j = 0;
            holderX1 = 0;
            holderX2 = 0;
            holderY1 = 0;
            holderY2 = 0;
            while (i < mNumExpandingBalls) {
                if (mNumExpandingBalls > 0) {
                    int holderRadius1 = maExplodingSprites.get(i).mBitmapImage.getHeight() / 2;
                    holderX1 = maExplodingSprites.get(i).mLeftCoordinate + holderRadius1;
                    holderY1 = maExplodingSprites.get(i).mTopCoordinate + holderRadius1;

                    while (j < mNumMovingBalls) {
                        int holderRadius2 = maMovingSprites.get(j).mBitmapImage.getHeight() / 2;
                        holderX2 = maMovingSprites.get(j).mLeftCoordinate + holderRadius2;
                        holderY2 = maMovingSprites.get(j).mTopCoordinate + holderRadius2;
                        int squared = ((holderX1 - holderX2) * (holderX1 - holderX2)) +
                                ((holderY1 - holderY2) * (holderY1 - holderY2));

                        if (Math.sqrt((double) squared) <= (double) ((holderRadius1) +
                                (holderRadius2))) {
                            maExplodingSprites.add(new ExpandingSprite(mContextRef,
                                    mDisplayRef, maMovingSprites.get(j).getCircleIndex(),
                                    maMovingSprites.get(j).mTopCoordinate,
                                    maMovingSprites.get(j).mLeftCoordinate,
                                    (int) BALL_STARTING_SCALE));
                            maMovingSprites.remove(j);
                            mNumMovingBalls--;

                            mNumExpandingBalls++;
                            mLevelScore += SCORE_INCRAMENTS;
                        } else {
                            j++;
                        }
                    }
                    j = 0;
                }
                if (maExplodingSprites.get(i).checkForDeletion()) {
                    maExplodingSprites.remove(i);
                    mNumExpandingBalls--;
                } else {
                    i++;
                }
            }

            for (int k = 0; k < mNumExpandingBalls; k++) {
                maExplodingSprites.get(k).explode(STANDARD_SCALE);
            }
            for (int k = 0; k < mNumMovingBalls; k++) {
                maMovingSprites.get(k).move();
            }

        }
    }

    /**
     * check level status returns if the level is over or not
     */

    public boolean checkLevelStatus() {
        return mbLevelOver;
    }

    /**
     * check win returns if the number of moving balls is less than or equal to the number of balls
     * to win
     */

    public boolean checkWIn() {
        return (mNumMovingBalls <= mNumBallsToWin);
    }

    /**
     * restart level simply restarts the current level with the current difficulty
     */

    public void restartLevel() {
        mLevelScore = 0;
        mbLevelOver = false;
        mbBallPlaced = false;
        mNumBallsToWin = mCurrentDifficulty + BASE_NUM_BALLS_TO_WIN;
        mNumExpandingBalls = -1;
        if (BALL_STARTING_SCALE == BALL_OLD_STARTING_SCALE) {
            setBallStartingScale();
        }
        if (mCurrentDifficulty <= LEVEL_BEFORE_SLOWDOWN) {
            mNumMovingBalls = (mCurrentDifficulty + 1) * BALL_DIFF_MULTIPLIER;
        } else {
            mNumMovingBalls = 0;
            mNumMovingBalls += LEVEL_BEFORE_SLOWDOWN * BALL_DIFF_MULTIPLIER;
            mNumMovingBalls += (BALL_DIFF_MULTIPLIER * (mCurrentDifficulty /
                    LEVELS_PER_BALL_CHANGE));
        }
        mNumBallsToWin = mNumMovingBalls - mNumBallsToWin;
        for (int i = 0; i < mNumMovingBalls; i++) {
            maMovingSprites.add(new BoundedMovingSprite(mContextRef,
                    mDisplayRef, getRandomDrawable(),
                    mRand.nextInt(mContextRef.getResources().getDisplayMetrics().heightPixels
                            - (int) BALL_STARTING_SCALE -
                            mContextRef.getResources().getDisplayMetrics().heightPixels / 9),
                    mRand.nextInt(mContextRef.getResources().getDisplayMetrics().widthPixels
                            - (int) BALL_STARTING_SCALE), (int) BALL_STARTING_SCALE));
        }

    }

    /**
     * returns the index of a random drawable
     */

    private int getRandomDrawable() {
        int returnVal = 0;
        int holder = (mRand.nextInt(MAX_RANDOM)) + 1;
        if (holder == MAX_RANDOM) {
            returnVal = R.drawable.specialcircle;
        } else {
            holder = (mRand.nextInt(MAX_NUM_RANDOM_COLORS));
            switch (holder) {
                case 0:
                    returnVal = R.drawable.bluecircle;
                    break;
                case 1:
                    returnVal = R.drawable.greencircle;
                    break;
                case 2:
                    returnVal = R.drawable.orangecircle;
                    break;
                case 3:
                    returnVal = R.drawable.redcircle;
                    break;
                case 4:
                    returnVal = R.drawable.yellowcircle;
                    break;
                case 5:
                    returnVal = R.drawable.pinkcircle;
                    break;

            }
        }

        return returnVal;

    }

    /**
     * down Expand Speed actualy increases the speed at which expanding sprites expand
     */

    public void upExpandSpeed() {
        STANDARD_SCALE++;
    }

    /**
     * setBallStartingScale sets the starting scale of all balls by calculating it based on a
     * ration from the screen width and height.
     */

    private double setBallStartingScale() {
        double ratio;
        if (mContextRef.getResources().getDisplayMetrics().widthPixels >
                mContextRef.getResources().getDisplayMetrics().heightPixels) {
            ratio = mContextRef.getResources().getDisplayMetrics().widthPixels * JUST_ONE
                    / mContextRef.getResources().getDisplayMetrics().heightPixels * JUST_ONE;
        } else {
            ratio = (mContextRef.getResources().getDisplayMetrics().heightPixels * JUST_ONE)
                    / (mContextRef.getResources().getDisplayMetrics().widthPixels * JUST_ONE);
        }

        BALL_STARTING_SCALE = ratio * BALL_OLD_STARTING_SCALE;
        return ratio;
    }

    /**
     * get num balls to win returns the number of balls left that is required to get a win
     */

    public int getmNumBallsToWin() {
        return mNumMovingBalls - mNumBallsToWin;
    }

    /**
     * printSprites prints all of the active circles in the level
     */

    public void printSpirtes(Canvas canvas) {
        for (int k = 0; k < mNumExpandingBalls; k++) {
            maExplodingSprites.get(k).doDraw(canvas);
        }
        for (int k = 0; k < mNumMovingBalls; k++) {
            maMovingSprites.get(k).doDraw(canvas);
        }
    }
}
