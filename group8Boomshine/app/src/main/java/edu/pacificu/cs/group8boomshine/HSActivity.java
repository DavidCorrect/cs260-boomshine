package edu.pacificu.cs.group8boomshine;

/**
 * Activity for viewing all of the high scores
 *
 * @author David Wright
 * @version 1.0
 */

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class HSActivity extends AppCompatActivity {
    private Button mBackBtn;
    private TextView mHsTextNumbers;
    private TextView mHsTextScores;
    private TextView mHsTextPlayers;
    private QueryHolder mHighScores;
    private DbWraper mDb;

    /**
     * Initiation creation
     *
     * @param savedInstanceState previous instance
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hs);

        mHsTextNumbers = findViewById(R.id.tvNumbers);
        mHsTextPlayers = findViewById(R.id.tvPlayers);
        mHsTextScores = findViewById(R.id.tvScores);

        mBackBtn = findViewById(R.id.btnBack);
        mBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(HSActivity.this, MainActivity.class));
                finish();
            }
        });

        mDb = new DbWraper(this);
        mHighScores = mDb.queryHighScores();

        for (int i = 0; i < mHighScores.scores.size(); i++) {
            mHsTextNumbers.append(Integer.toString(i + 1) + ". \n");
            mHsTextPlayers.append(mHighScores.players.get(i) + "\n");
            mHsTextScores.append(Integer.toString(mHighScores.scores.get(i)) + "\n");
        }
    }

    /**
     * Actions to preform on object destruction
     */
    @Override
    protected void onDestroy() {
        this.mDb.mDbHelper.close();

        super.onDestroy();
    }
}
