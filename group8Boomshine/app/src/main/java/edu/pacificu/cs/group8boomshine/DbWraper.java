package edu.pacificu.cs.group8boomshine;

/**
 * Wrapper class for the database to query and insert
 *
 * @author David Wright
 * @version 1.0
 */

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class DbWraper {
    private final int FAIL = -1;
    protected HSDbHelper mDbHelper;
    protected SQLiteDatabase mDb;


    /**
     * intializes helper object
     *
     * @param context current activity
     */
    public DbWraper(Context context) {
        mDbHelper = new HSDbHelper(context);
    }

    /**
     * inserts score and player into the database
     *
     * @param score  player score
     * @param player player name
     * @return if the insert was successful
     */
    protected boolean insertScore(int score, String player) {
        // Gets data repository in write mode
        mDb = mDbHelper.getWritableDatabase();

        //Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(HSContract.HighScores.COLUMN_NAME_SCORE, score);
        values.put(HSContract.HighScores.COLUMN_NAME_PLAYER, player);

        //insert returns RowId or -1 if it fails
        return FAIL != mDb.insert(HSContract.HighScores.TABLE_NAME, null, values);
    }

    /**
     * Queries database for all stored scores and players
     *
     * @return QueryHolder object containing organized player names and scores
     */
    protected QueryHolder queryHighScores() {
        QueryHolder cHolder = new QueryHolder();

        // Get data repository in read mode
        mDb = mDbHelper.getReadableDatabase();

        //Projection defines which columns you will use from the query
        String[] projection = {
                HSContract.HighScores.COLUMN_NAME_PLAYER,
                HSContract.HighScores.COLUMN_NAME_SCORE
        };

        // Sorting
        String sortOrder = HSContract.HighScores.COLUMN_NAME_SCORE + " DESC";

        Cursor cursor = mDb.query
                (
                        HSContract.HighScores.TABLE_NAME,  // Table
                        projection,                        // Array of columns to get (null for all)
                        null,                      // Columns in WHERE
                        null,                   // Values for WHERE
                        null,                      // GROUP BY
                        null,                       // HAVING
                        sortOrder                          // Sort order
                );

        //Cursor always starts at -1 so moveToNext() must always be called
        while (cursor.moveToNext()) {
            cHolder.players.add(cursor.getString
                    (cursor.getColumnIndexOrThrow(HSContract.HighScores.COLUMN_NAME_PLAYER)));
            cHolder.scores.add(cursor.getInt
                    (cursor.getColumnIndexOrThrow(HSContract.HighScores.COLUMN_NAME_SCORE)));
        }
        cursor.close();

        return cHolder;
    }

    /**
     * Testing function for dropping the database when needing a fresh start
     */
    private void dropTable() {
        mDb = mDbHelper.getReadableDatabase();

        String selection = HSContract.HighScores._ID + "  > ?";

        String[] selectionArgs = {"0"};

        mDb.delete(HSContract.HighScores.TABLE_NAME, selection, selectionArgs);
    }
}
