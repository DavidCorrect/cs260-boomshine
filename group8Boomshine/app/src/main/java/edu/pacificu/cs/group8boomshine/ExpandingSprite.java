package edu.pacificu.cs.group8boomshine;

/**
 * Defines the ExpandingSprite class and all of the data and methods associated with it.
 *
 * @author Logan Jepson, David Wright
 * @version 1.0
 */

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.view.Display;

public class ExpandingSprite extends FixedSprite {
    static private int OLD_EXPANDING_SPRITE_MAX_SIZE = 300;
    static private int EXPANDING_SPRITE_MAX_SIZE = 300;
    static private int MIN_SCALE = 20;
    static private double JUST_ONE = 1.0;
    static private int RATIO_DIV = 100;

    private Matrix mMatrix;
    private Bitmap mOriginalBitmap = mBitmapImage;
    private boolean bExpanding;
    private boolean bStopResizing;
    private int mNewWidth;
    private int mNewHeight;

    /**
     * Constructor that initializes the values and game
     * object of the Expanding Sprite class.
     *
     * @param context
     *          reference to application-specific resources
     *
     * @param display
     *          the display
     *
     *@param drawable
     *          tag to a drawable sprite
     *
     * @param topCoord
     *         top coord where the sprite will be placed
     *
     * @param leftCoord
     *         left coord where the sprite will be placed
     *
     * @param circleStartingScale
     *         the scale that the bounded sprite will start at
     *
     *
     * @since 1.0
     */

    public ExpandingSprite(Context context, Display display, int drawable,
                           int topCoord, int leftCoord, int circleStartingScale) {
        super(context, display, drawable, topCoord, leftCoord);
        mBitmapImage = Bitmap.createScaledBitmap
                (mOriginalBitmap, circleStartingScale, circleStartingScale, true);
        mNewWidth = super.getSpriteWidth();
        mNewHeight = super.getSpriteHeight();
        updateDimensions();
        bExpanding = true;
        bStopResizing = false;

        if (EXPANDING_SPRITE_MAX_SIZE == OLD_EXPANDING_SPRITE_MAX_SIZE ) {
            double ratio = (context.getResources().getDisplayMetrics().widthPixels * JUST_ONE) +
                    (context.getResources().getDisplayMetrics().widthPixels * JUST_ONE );
            ratio /= RATIO_DIV;
            ratio += OLD_EXPANDING_SPRITE_MAX_SIZE;
            EXPANDING_SPRITE_MAX_SIZE = (int)ratio;
        }
    }

    /**
     * explode resizes the sprites while also moving them to account for the resize
     *
     * @param scale
     *          the scale at which the sprite will be increasing by
     *
     *
     * @since 1.0
     */

    protected void explode(int scale) {
        checkBounds();
        if (bExpanding && !bStopResizing) {
            mBitmapImage = getResizedBitmap
                    (mOriginalBitmap, mBitmapImage.getHeight(), mBitmapImage.getWidth(), scale);
            updateDimensions();
            mTopCoordinate -= scale / 2;
            mLeftCoordinate -= scale / 2;
        } else if (!bStopResizing) {
            mBitmapImage = getResizedBitmap
                    (mOriginalBitmap, mBitmapImage.getHeight(), mBitmapImage.getWidth(), scale);
            updateDimensions();
            mTopCoordinate += scale / 2;
            mLeftCoordinate += scale / 2;
        }

    }


    /**
     * getResizedBitmap retrieves a bitmap that is
     * scaled up or down a certain ammount from the original
     *
     * @param bm
     *        the given bitmap that is going to be resized
     *
     * @param newHeight
     *         the new height that will be given to the resized bitmap
     *
     * @param newWidth
     *         the new width that will be given to the resized bitmap
     *
     * @param scale
     *         the new scale that will be given to the resized bitmap
     *
     *
     * @since 1.0
     */

    public Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth, int scale) {

        int width = bm.getWidth();

        int height = bm.getHeight();

        if (bExpanding) {
            newHeight += scale;
            newWidth += scale;
        } else {
            newHeight -= scale;
            newWidth -= scale;
        }

        float scaleWidth = ((float) newWidth) / width;

        float scaleHeight = ((float) newHeight) / height;

        // CREATE NEW MATRIX

        mMatrix = new Matrix();

        // RESIZE THE BIT MAP

        mMatrix.postScale(scaleWidth, scaleHeight);

        // RECREATE THE NEW BITMAP

        Bitmap resizedBitmap = Bitmap.createBitmap
                (bm, 0, 0, width, height, mMatrix, false);
        mNewWidth = resizedBitmap.getWidth();
        mNewHeight = resizedBitmap.getHeight();
        return resizedBitmap;

    }

    /**
     * checkBounds checks to see if the sprite should stop expanding
     *
     *
     * @since 1.0
     */

    private void checkBounds() {
        if (mBitmapImage.getWidth() >= EXPANDING_SPRITE_MAX_SIZE) {
            bExpanding = false;
        }
        if (mBitmapImage.getWidth() <= MIN_SCALE) {
            bStopResizing = true;
        }
    }

    /**
     * getSpriteWidth returns the resized sprites width
     *
     *
     * @since 1.0
     */

    @Override
    public int getSpriteWidth() {
        return mNewWidth;
    }

    /**
     * getSpriteHeight returns the resized sprites height
     *
     *
     * @since 1.0
     */

    @Override
    public int getSpriteHeight() {
        return mNewHeight;
    }

    /**
     * check for deletion determines if the expanding sprite should be deleted because it isn't
     * being resized
     *
     *
     * @since 1.0
     */

    public boolean checkForDeletion() {
        return bStopResizing;
    }
}
