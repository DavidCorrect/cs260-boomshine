package edu.pacificu.cs.group8boomshine;

/**
 * Activity for when the player loses the game. Gives prompt to input their nplayer name for
 * the high score list
 *
 * @author David Wright
 * @version 1.0
 */

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class LoseActivity extends AppCompatActivity {
    private TextView mScoreText;
    private Button mSubmitBtn;
    private EditText mUsername;

    private DbWraper mDb;
    private int mScore;

    /**
     * Initialization on creation
     *
     * @param savedInstanceState previous instance
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lose);

        mScore = getIntent().getIntExtra("EXTRA_INT", 0);

        mDb = new DbWraper(this);

        mUsername = findViewById(R.id.etUsername);
        mScoreText = findViewById(R.id.tvDisplayScore);
        mScoreText.setText(Integer.toString(mScore));

        mSubmitBtn = findViewById(R.id.btnSubmit);
        mSubmitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mUsername.getText().toString().trim().equals("")) {
                    mDb.insertScore(mScore, mUsername.getText().toString());
                    startActivity(new Intent(LoseActivity.this, HSActivity.class));
                    finish();
                }
            }
        });
    }
}
