package edu.pacificu.cs.group8boomshine;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.Display;

import java.util.Random;

/**
 * Defines the bounded moving sprite class with all of the data and methods associated with it.
 *
 * @author Logan Jepson, David Wright
 * @version 1.0
 */

public class BoundedMovingSprite extends MovingSprite {
    static private double BASE_SPEED = 10;
    static private int BOUND_MULTIPLIER = 2;
    static private int HEIGHT_DIV = 9;
    double MoveX, MoveY;
    int LeftWall, RightWall, TopWall, BottomWall;
    private int mDrawableInex;
    Random mRand = new Random(Double.doubleToLongBits(Math.random()));

    /**
     * Constructor that initializes the values and game object of the Bounded Moving Sprite class.
     *
     * @param context   reference to application-specific resources
     * @param display   the display
     * @param drawable  tag to a drawable sprite
     * @param topCoord  top coord where the sprite will be placed
     * @param leftCoord left coord where the sprite will be placed
     * @since 1.0
     */

    public BoundedMovingSprite(Context context, Display display, int drawable, int topCoord, int leftCoord) {
        super(context, display, drawable, topCoord, leftCoord);

        MoveX = (mRand.nextInt() % BASE_SPEED);
        MoveY = (mRand.nextInt() % BASE_SPEED);
        MoveX++;
        MoveY++;
        LeftWall = 0;
        TopWall = 0;
        RightWall = getDisplayWidth() - getSpriteWidth();
        BottomWall = getDisplayHeight() - getSpriteHeight() - getDisplayHeight() / HEIGHT_DIV ;
        mDrawableInex = drawable;
        if (topCoord + getSpriteHeight() >= BottomWall) {
            mTopCoordinate = topCoord - getSpriteHeight() * BOUND_MULTIPLIER; //don't need const for 2
        }

        if (leftCoord + getSpriteWidth() >= RightWall) {
            mLeftCoordinate = leftCoord - getSpriteWidth() * BOUND_MULTIPLIER;
        }

    }

    /**
     * Constructor that initializes the values and game object of the Bounded Moving Sprite class.
     *
     * @param context       reference to application-specific resources
     * @param display       the display
     * @param drawable      tag to a drawable sprite
     * @param topCoord      top coord where the sprite will be placed
     * @param leftCoord     left coord where the sprite will be placed
     * @param startingScale the scale that the bounded sprite will start at
     * @since 1.0
     */

    public BoundedMovingSprite(Context context, Display display,
                               int drawable, int topCoord, int leftCoord, int startingScale) {
        super(context, display, drawable, topCoord, leftCoord);

        MoveX = (mRand.nextInt() % BASE_SPEED);
        MoveY = (mRand.nextInt() % BASE_SPEED);
        MoveX++;
        MoveY++;
        LeftWall = 0;
        TopWall = 0;
        mDrawableInex = drawable;
        resizeSprite(startingScale);
        updateDimensions();
        RightWall = getDisplayWidth() - getSpriteWidth();
        BottomWall = getDisplayHeight() - getSpriteHeight() - getDisplayHeight() / HEIGHT_DIV ;
        //guess and check
        if (topCoord + getSpriteHeight() >= BottomWall) {
            mTopCoordinate = topCoord - getSpriteHeight() * BOUND_MULTIPLIER; //don't need const for 2
        }

        if (leftCoord + getSpriteWidth() >= RightWall) {
            mLeftCoordinate = leftCoord - getSpriteWidth() * BOUND_MULTIPLIER;
        }

    }

    /**
     * move moves the bounded moving sprite while checking for wall collisions.
     *
     * @since 1.0
     */

    @Override
    public void move() {
        changeDirection();

        mLeftCoordinate += MoveX;
        mTopCoordinate += MoveY;
    }

    /**
     * changeDirection inverts the x or y coordinate depending on what wall is collided with
     *
     * @since 1.0
     */

    private void changeDirection() {
        if (mLeftCoordinate <= LeftWall) {
            MoveX *= -1;
        }
        if (mLeftCoordinate + getSpriteWidth() >= RightWall) {
            MoveX *= -1;
        }
        if (mTopCoordinate <= TopWall) {
            MoveY *= -1;
        }
        if (mTopCoordinate + getSpriteHeight() >= BottomWall) {
            MoveY *= -1;
        }
    }

    /**
     * resizeSprite changes the sprite to a given scale
     *
     * @param circleStartingScale the scale that the circle sprite starts at.
     * @since 1.0
     */

    private void resizeSprite(int circleStartingScale) {
        mBitmapImage = Bitmap.createScaledBitmap(mBitmapImage,
                circleStartingScale, circleStartingScale, true);
    }

    public int getCircleIndex () {
        return mDrawableInex;
    }

}
