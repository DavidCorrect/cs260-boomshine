package edu.pacificu.cs.group8boomshine;

/**
 * Defines the View for displaying the Boomshine game. This also runs the Boomshine game object
 *
 * @author Logan Jepson, David Wright
 * @version 1.0
 */

import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Build;
import android.view.Display;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.RequiresApi;

public class BoomshineView extends View {
    static private int mUILength;
    static private int mUIHeight;
    static private int UI_DIV = 18;
    static private int DIV_NUM = 2;
    static private int MAX_NUM_TRYS = 3;
    protected DbWraper mDb;
    MainActivity mMainActivity;
    String mHolder;
    BoomshineGame cmNewGame;
    Display mDisplayRef;
    private boolean bGameOver;
    private Paint mBackground = new Paint();
    private Paint mTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private int levelStatus = BoomshineGame.ONGOING;

    /**
     * Constructor that initializes the values and game object of the boomshine view class.
     *
     * @param context reference to application-specific resources
     * @param display the display
     * @since 1.0
     */

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    public BoomshineView(Context context, Display display) {
        super(context);
        mDisplayRef = display;
        // Gives access to MainActivity resources
        this.mMainActivity = (MainActivity) context;
        setFocusable(true);
        setFocusableInTouchMode(true);
        bGameOver = false;

        //check here if gives problems
        cmNewGame = new BoomshineGame(context, display);
        mDb = new DbWraper(context);
    }

    /**
     * onDraw accepts a canvas, draws all gameplay, and check for certain game conditions
     *
     * @param canvas Object that calls draw methods for the screen
     */
    protected void onDraw(Canvas canvas) {
        mUIHeight = getHeight() / UI_DIV;
        mUILength = getWidth() / UI_DIV;

        //setPaint and such
        mTextPaint.setColor(Color.GRAY);
        mTextPaint.setStyle(Paint.Style.FILL);
        mTextPaint.setTextSize(mUIHeight * 0.75f);

        mTextPaint.setTextScaleX(mUIHeight / mUILength);


        mBackground.setColor(getResources().getColor(R.color.cBlack));
        canvas.drawRect(0, 0, getWidth(), getHeight(), mBackground);
        if (!bGameOver) {
            if (!(levelStatus == BoomshineGame.ONGOING)) {
                if (levelStatus == BoomshineGame.WON) {
                    cmNewGame.goToNextLevel();
                    levelStatus = BoomshineGame.ONGOING;
                } else {
                    Intent intent = new Intent(mMainActivity, LoseActivity.class);
                    intent.putExtra("EXTRA_INT", cmNewGame.getGameScore());

                    cmNewGame = new BoomshineGame(super.getContext(), mDisplayRef);
                    levelStatus = BoomshineGame.ONGOING;

                    bGameOver = true;

                    bGameOver = false;
                    mMainActivity.startActivity(intent);
                }
            } else {
                levelStatus = cmNewGame.runLevel();
                cmNewGame.printSprites(canvas);
            }
        }

        // Print Level
        mTextPaint.setTextAlign(Paint.Align.LEFT);
        Paint.FontMetrics fontMetrics = mTextPaint.getFontMetrics();
        float xFontCoord = 0;
        float yFontCoord = (mUIHeight / 2) - (fontMetrics.ascent + fontMetrics.descent) / DIV_NUM ;
        mHolder = "Current Level: " + (cmNewGame.getLevel() + 1);
        canvas.drawText(mHolder, xFontCoord, yFontCoord, mTextPaint);

        //Print Attempt
        mTextPaint.setTextAlign(Paint.Align.RIGHT);
        mHolder = "Attempts: " + (MAX_NUM_TRYS  - cmNewGame.getAttempt());
        xFontCoord = getContext().getResources().getDisplayMetrics().widthPixels;
        canvas.drawText(mHolder, xFontCoord, yFontCoord, mTextPaint);

        // Print Score
        mTextPaint.setTextAlign(Paint.Align.LEFT);
        xFontCoord = 0;
        yFontCoord = getHeight() +
                (fontMetrics.ascent + fontMetrics.descent) / DIV_NUM ;
        mHolder = "Current Score: " + (cmNewGame.getGameScore());
        canvas.drawText(mHolder, xFontCoord, yFontCoord, mTextPaint);

        // Print Must Hit
        mTextPaint.setTextAlign(Paint.Align.RIGHT);
        if (cmNewGame.getNumBallsToWin() >= 0) {
            mHolder = "Must hit: " + (cmNewGame.getNumBallsToWin());
        } else {
            mHolder = "Must hit: " + (0);
        }
        xFontCoord = getContext().getResources().getDisplayMetrics().widthPixels;
        canvas.drawText(mHolder, xFontCoord, yFontCoord, mTextPaint);

        invalidate();

    }

    /**
     * onTouchEvent handles what happens when the screen is touched. Runs the minesweeper logic
     *
     * @param event Motion for screen being touched
     */
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int holderX, holderY;
        // Prevents onTouch being called from ACTION_UP & ACTION_DOWN
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            holderX = (int) event.getX();
            holderY = (int) event.getY();
            if (!bGameOver) {
                cmNewGame.userPlacedCircle(holderY, holderX);
            }
        }

        return true;
    }
}
