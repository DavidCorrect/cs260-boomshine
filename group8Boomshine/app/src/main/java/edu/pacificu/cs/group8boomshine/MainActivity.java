package edu.pacificu.cs.group8boomshine;

/**
 * Defines the Main Activity class that runs the entire android app and switches between activities
 *
 * @author Logan Jepson, David Wright
 * @version 1.0
 */

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.WindowManager;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    private BoomshineView mBoomView;
    private Display mDisplay;

    /**
     * Initialization for when app starts
     *
     * @param savedInstanceState
     */
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        WindowManager window = getWindowManager();

        mDisplay = window.getDefaultDisplay();
        mBoomView = new BoomshineView(this, mDisplay);
        setContentView(mBoomView);
    }

    @Override
    protected void onDestroy() {
        mBoomView.mDb.mDbHelper.close();
        super.onDestroy();
    }


    /**
     * Initializes for Menu
     *
     * @param menu
     */

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    /**
     * Handles menu item selections
     *
     * @param item
     */

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.menuHS:
                Log.d("onOptionsItemSelected", "HighScores");
                startActivity(new Intent(MainActivity.this, HSActivity.class));
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
