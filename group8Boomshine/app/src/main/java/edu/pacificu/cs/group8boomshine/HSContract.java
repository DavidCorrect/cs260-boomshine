package edu.pacificu.cs.group8boomshine;

/**
 * Static fields to easily propagate any changes down through the database
 *
 * @author David Wright
 * @version 1.0
 */

import android.provider.BaseColumns;

public final class HSContract {
    //Private to prevent instantiation of contract class
    private HSContract() {
    }

    //Inner class to define table contents
    public static class HighScores implements BaseColumns {
        public static final String TABLE_NAME = "HighScores";

        public static final String COLUMN_NAME_SCORE = "score";
        public static final String COLUMN_NAME_PLAYER = "player";
    }
}
